(function () {
	"use strict";
	
	/*global angular */
	angular
		.module('formlyModule', ['formly', 'formlyBootstrap', 'ui.bootstrap', 'ui.bootstrap.datetimepicker'])
		.run(function (formlyConfig) {
			var attributes = [
					'date-disabled',
					'custom-class',
					'show-weeks',
					'starting-day',
					'init-date',
					'min-mode',
					'max-mode',
					'format-day',
					'format-month',
					'format-year',
					'format-day-header',
					'format-day-title',
					'format-month-title',
					'year-range',
					'shortcut-propagation',
					'show-button-bar',
					'current-text',
					'clear-text',
					'close-text',
					'close-on-date-selection',
					'datepicker-append-to-body',
					'meridians',
					'readonly-input',
					'mousewheel',
					'arrowkeys'
				],
				bindings = [
					'datepicker-mode',
					'min-date',
					'max-date',
					'hour-step',
					'minute-step',
					'show-meridian'
				],
				ngModelAttrs = {};

			formlyConfig.setType({
				"name": 'datepicker',
				"templateUrl": 'datepicker.html',
				"wrapper": ['bootstrapLabel', 'bootstrapHasError'],
				"defaultOptions": {
					"ngModelAttrs": ngModelAttrs,
					"templateOptions": {
						"datepickerOptions": {
							"format": 'dd-MM-yyyy HH:mm'
						}
					}
				},
				"controller": ['$scope', function ($scope) {
					$scope.datepicker = {};
					$scope.datepicker.opened = false;
					$scope.datepicker.open = function ($event) {
						$scope.datepicker.opened = true;
					};
				}]
			});

			/**
			* camelize
			* @param {} string
			* @return {}
			*/
			function camelize(string) {
				string = string.replace(/[\-_\s]+(.)?/g, function (match, chr) {
					return chr ? chr.toUpperCase() : '';
				});
				// Ensure 1st char is always lowercase
				return string.replace(/^([A-Z])/, function (match, chr) {
					return chr ? chr.toLowerCase() : '';
				});
			}

			angular.forEach(attributes, function (attr) {
				ngModelAttrs[camelize(attr)] = {attribute: attr};
			});

			angular.forEach(bindings, function (binding) {
				ngModelAttrs[camelize(binding)] = {bound: binding};
			});

			angular.element(document).ready(function () {
				angular.bootstrap(document, ['formlyModule']);
			});
		});
}());
