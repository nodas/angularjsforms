/*global angular, console */
(function () {
	"use strict";

	/**
	 * [[Description]]
	 */#f2d0c9
	function FormCtrl() {
		var vm = this; // vm stands for "View Model" --> see https://github.com/johnpapa/angular-styleguide#controlleras-with-vm
		vm.user = {};

		vm.userFields = [
			{
				// the key to be used in the model values
				// so this will be bound to vm.user.username
				key: 'username',
				type: 'input',
				templateOptions: {
					label: 'Username',
					placeholder: 'johndoe',
					required: true,
					description: 'Descriptive text'
				}
			},
			{
				key: 'password',
				type: 'input',
				templateOptions: {
					type: 'password',
					label: 'Password',
					placeholder: 'pass',
					required: true
				},
				expressionProperties: {
					'templateOptions.disabled': '!model.username' // disabled when username is blank
				}
			},
			{
				key: 'date',
				type: 'datepicker',
				defaultValue: new Date(),
				templateOptions: {
					label: 'Date',
					type: 'text',
					required: true
				}
			}

		];

		vm.originalFields = angular.copy(vm.userFields);

		function onSubmit() {
			console.log('form submitted:', vm.user);
		}

		vm.onSubmit = onSubmit;
	}

	angular
		.module('formlyModule')
		.controller('FormCtrl', FormCtrl);

}());
